package April01;

public abstract class Hewan {
    public abstract int getNumberOfLegs();
    public abstract String getFavoriteFood();
}
