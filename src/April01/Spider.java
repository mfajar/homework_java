package April01;

public class Spider extends Hewan{
    @Override
    public int getNumberOfLegs() {
        return 8;
    }

    @Override
    public String getFavoriteFood() {
        return "Serangga";
    }
}
