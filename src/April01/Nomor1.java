package April01;

public class Nomor1 {
    public static void main(String[] args) {
        Hewan[] hewan = new Hewan[6];
        hewan[0] = new Dog();
        hewan[1] = new Spider();
        hewan[2] = new Fly();
        hewan[3] = new Centipede();
        hewan[4] = new Snake();
        hewan[5] = new Chiken();

        for (int i=0;i< hewan.length;i++){
            System.out.println("Jumlah Kaki "+hewan[i].getClass().getSimpleName()+" : "+hewan[i].getNumberOfLegs());
            System.out.println("Makanan Favorite "+hewan[i].getClass().getSimpleName()+" : "+hewan[i].getFavoriteFood());
        }
    }
}
