package April01;

public class Nomor2 {
    public static void main(String[] args) {
        Account account = new Account("Fajar","10234");
        System.out.println(account);
        System.out.println();

        SavingAccount savingAccount = new SavingAccount("Muhammad Fajar","107211",325.45);
        System.out.println(savingAccount);
        savingAccount.deposit(150);
        System.out.println(savingAccount);
        savingAccount.withdraw(100);
        System.out.println(savingAccount);

    }
}
