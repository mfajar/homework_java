package April01;

public class SavingAccount extends Account{
    private double balance;

    public SavingAccount(String name, String accountNumber,double balance) {
        super(name, accountNumber);
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void deposit(double deposit){
        this.balance += deposit;
    }
    public void withdraw(double wd){
        this.balance -= wd;
    }

    @Override
    public String toString() {
        return accountNumber+"\n"+name+"\n$"+balance;
    }
}
