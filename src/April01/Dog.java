package April01;

public class Dog extends Hewan{

    @Override
    public int getNumberOfLegs() {
        return 4;
    }

    @Override
    public String getFavoriteFood() {
        return "Daging";
    }
}
