package April01;

public class Centipede extends Hewan{
    @Override
    public int getNumberOfLegs() {
        return 100;
    }

    @Override
    public String getFavoriteFood() {
        return "Daun";
    }
}
