package Mar29;

import java.util.Scanner;

public class Number1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Variable : ");
        String text = in.nextLine().toLowerCase();

        for (char x ='a' ; x<='z';x++ ){
            int charCount = 0;
            for (int i=0;i<text.length();i++){
                if(x == text.charAt(i)){
                    charCount++;
                }
            }
            if(charCount>0)
            System.out.println("Alfabet "+x+" muncul "+charCount+" kali");
        }

    }
}
