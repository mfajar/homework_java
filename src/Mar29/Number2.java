package Mar29;

import java.util.Scanner;

public class Number2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan Kalimat : ");
        String text = in.nextLine();
        String result ="";
        for (int i = text.length()-1 ; i>=0 ; i--){
         result = result.concat(String.valueOf(text.charAt(i)));
        }
        System.out.println("Kalimat Terbalik = "+result);
    }
}
